otpauth-uri-parser
==================
A simple function for parsing `otpauth://` URIs.

It uses the global `URLSearchParams` object, so this will only work in modern browsers and node >= 10
or with an appropriate polyfill.

This is based on [the specs here](https://github.com/google/google-authenticator/wiki/Key-Uri-Format) so
there might be some corner cases not covered.


Usage
-----
It's pretty simple.

```js
const parseURI = require('otpauth-uri-parser');

const parsed = parseURI('otpauth://totp/Example:alice@google.com?secret=JBSWY3DPEHPK3PXP&issuer=Example');

console.log(parsed);

// Gives us:
{
    type: 'totp',
    label: { issuer: 'Example', account: 'alice@google.com' },
    query: { secret: 'JBSWY3DPEHPK3PXP', issuer: 'Example' }
}
```

__Note:__ numeric query string parameters are not converted to numbers. So if the URI has `digits=6` you
will get `{ digits: '6' }` in the `query` object and not `{ digits: 6 }`;


Contributing
------------
If you encounter an `otpauth://` URI that this fails to parse as expected please open an issue and
ideally create a merge request with a failing test case.
