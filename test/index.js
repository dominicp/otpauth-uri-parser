/**
 * Basic test suite for the app
 */
const { expect } = require('chai');
const parseURI = require('../');

describe('otpauth-uri-parser', function () {

    it('should rock', function () { return true; });

    const goodURIs = [
        [
            'otpauth://totp/Example:alice@google.com?secret=JBSWY3DPEHPK3PXP&issuer=Example',
            {
                type: 'totp',
                label: { issuer: 'Example', account: 'alice@google.com' },
                query: { secret: 'JBSWY3DPEHPK3PXP', issuer: 'Example' }
            },
        ], [
            'otpauth://totp/ACME%20Co:john.doe@email.com?secret=HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ&issuer=ACME%20Co&algorithm=SHA1&digits=6&period=30',
            {
                type: 'totp',
                label: { issuer: 'ACME Co', account: 'john.doe@email.com' },
                query: { secret: 'HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ', issuer: 'ACME Co', algorithm: 'SHA1', digits: '6', period: '30' }
            },
        ], [
            'otpauth://TYPE/LABEL?PARAMETERS',
            {
                type: 'type',
                label: { issuer: '', account: 'LABEL' },
                query: { PARAMETERS: '' }
            }
        ], [
            'otpauth://HTOP/Account-Name?secret=shhhh',
            {
                type: 'htop',
                label: { issuer: '', account: 'Account-Name' },
                query: { secret: 'shhhh' }
            }
        ], [
            'otpauth://topt/Big%20Corporation%3A%20alice%40bigco.com?secret=shhhh',
            {
                type: 'topt',
                label: { issuer: 'Big Corporation', account: 'alice@bigco.com' },
                query: { secret: 'shhhh' }
            }
        ], [
            'otpauth://topt/Big%20Corporation%3A%20alice%40bigco.com',
            {
                type: 'topt',
                label: { issuer: 'Big Corporation', account: 'alice@bigco.com' },
                query: { }
            }
        ]
    ];

    const badURIs = [
        'this is not a uri',
        'http://example.com',
        { otpauth: 'broken' },
        'otpauth://topt/'
    ];


    goodURIs.forEach(([uri, expected]) => {

        it('should parse: ' + uri, function () {

            expect(parseURI(uri)).to.deep.equal(expected);
        });
    });

    badURIs.forEach((uri) => {

        it('should not parse: ' + uri, function () {

            expect(parseURI(uri)).to.be.null;
        });
    });
});
